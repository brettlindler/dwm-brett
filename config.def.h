
/* appearance */
static const unsigned int borderpx  = 6;        /* border pixel of windows */
static const unsigned int gappx     = 16;       // gaps between windows
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int user_bh	        = 28;           // barheight value
static const char *fonts[]          = { "LiberationMono:size=11" };
static const char dmenufont[]       = "LiberationMono:size=11";
static const char col_gray1[]       = "#222222";  // unfocused bar/tags
static const char col_gray2[]       = "#000000";  // unfocused window
static const char col_gray3[]       = "#9f9f9f";  // tag text
static const char col_gray4[]       = "#eeeeee";  // focused text
static const char col_cyan[]        = "#005577";  // focused window
static const char *colors[][3]      = {
       /*               fg         bg         border   */
       [SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
       [SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};

/* tagging */
static const char *tags[] = { "[www]", "[pass]", "[dev]", "[steam]", "[lutris]", "[mus]", "[virt]", }; 

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
        /* class                     instance    title       tags mask     isfloating   monitor */
         { "Gimp",                    NULL,       NULL,       0,            1,           -1 },
         { "KeePassXC",               NULL,       NULL,       1 << 1,       0,           -1 },
         { "Firefox",                 NULL,       NULL,       1 << 8,       0,           -1 },
         { "Galculator",              NULL,       NULL,       0,            1,           -1 },
         { "Steam",                   NULL,       NULL,       1 << 3,       1,           -1 },
         { "Lutris",                  NULL,       NULL,       1 << 4,       0,           -1 },
         { "VirtualBox Manager",      NULL,       NULL,       1 << 6,       0,           -1 },
	 { "prboom-plus",	      NULL,       NULL,       0,	    1,		 -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "termite", NULL };
static const char *browser[]  = { "firefox", NULL };
static const char *lutris[]  = { "lutris", NULL };
static const char *steam[]  = { "steam", NULL };
static const char *musescore[]  = { "musescore", NULL };
static const char *emacs[] = { "emacs", NULL };
static const char *xkill[] = { "xkill", NULL };
static const char *deadbeef[] = { "deadbeef", NULL };
static const char *poweroff[] = { "poweroff", NULL };
static const char *reboot[] = { "reboot", NULL };
static const char *filemanager[] = { "pcmanfm", NULL };
static const char *qzdl[] = { "qzdl", NULL };
static const char *virtualbox[] = { "virtualbox", NULL };
static const char *keepassxc[] = { "keepassxc", NULL };
static const char *thunderbird[] = { "thunderbird", NULL };


static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },

    // Main DWM Controls

	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY|ShiftMask,             XK_j,      rotatestack,    {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      rotatestack,    {.i = -1 } },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,	                XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY|ControlMask,           XK_f,      togglefullscr,  {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },

    // Gaps

	{ MODKEY,                       XK_minus,  setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },

    // Tags

	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)

// Launch Programs
 
    { MODKEY|ShiftMask,             XK_w,      spawn,          {.v = browser } },           // Launch Firefox
    { MODKEY|ShiftMask,             XK_l,      spawn,          {.v = lutris } },            // Launch Lutris
    { MODKEY|ShiftMask,             XK_s,      spawn,          {.v = steam } },             // Launch Steam
    { MODKEY|ShiftMask,             XK_m,      spawn,          {.v = musescore } },         // Launch Musescore
    { MODKEY|ShiftMask,             XK_e,      spawn,          {.v = emacs } },             // Launch emacs
    { MODKEY|ShiftMask,             XK_d,      spawn,          {.v = deadbeef } },          // Launch Deadbeef
    { MODKEY|ShiftMask,             XK_f,      spawn,          {.v = filemanager} },        // Launch File Manager
    { MODKEY|ShiftMask,             XK_z,      spawn,          {.v = qzdl } },              // Launch QZdoom Launcher
    { MODKEY|ShiftMask,             XK_v,      spawn,          {.v = virtualbox } },        // Launch virtualbox
    { MODKEY|ShiftMask,             XK_k,      spawn,          {.v = keepassxc } },         // Launch keepassxc
    { MODKEY|ShiftMask,             XK_t,      spawn,          {.v = thunderbird } },       // Launch thunderbird
 
    // System Hotkeys

    { ControlMask|ShiftMask,        XK_x,      spawn,          {.v = xkill } },             // Launch xkill
    { MODKEY|ControlMask|ShiftMask|Mod1Mask, XK_p,      spawn,          {.v = poweroff } },          // Poweroff
    { MODKEY|ControlMask|ShiftMask, XK_r,      spawn,          {.v = reboot } },            // Reboot
    { MODKEY|ControlMask|ShiftMask|Mod1Mask,             XK_q,      quit,           {0} },                       // kill dwm
    { MODKEY|ControlMask|ShiftMask, XK_q,      quit,           {1} },                       // Restart dwm


};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

